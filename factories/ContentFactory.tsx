import React from "react";

import {IPageContext} from "../components/@def";
import {HomeItem} from "../components/HomeItem";
import {Iframe} from "../components/Iframe";
import {HTML} from "../components/HTML";
import {Img} from "../components/Img";
import {EnumerationItem} from "../components/EnumerationItem";
import {TileItem} from "../components/TileItem";
import {LinkSection} from "../components/LinkSection";
import {Events} from "../components/Events";
import {NoContent} from "../components/NoContent";
import {Timeline} from "../components/Timeline";
import {Calendar} from "../components/Calendar";



export class ContentFactory {
    public create(strategy: string, config: any, context: IPageContext): React.ReactNode {
        // const dispatcher = dispatch();
        // const $services = stateSelector(state => state.$services);
        switch (strategy) {
            case "home-item":
                return <HomeItem context={context} {...config} />;
            case "iframe":
                return <Iframe context={context} {...config} />;
            case "html":
                return <HTML context={context} {...config} />;
            case "img":
                return <Img context={context} {...config} />;
            case "enumeration-item":
                return <EnumerationItem context={context} {...config} />;
            case "tile-item":
                return <TileItem context={context} {...config} />;
            case "link-section":
                return <LinkSection context={context} {...config} />;
            case "events":
                return <Events context={context} {...config} />;
            case "timeline":
                return <Timeline context={context} {...config} />;
            case "calendar":
                return <Calendar context={context} {...config} />;
            case "no-content-available":
                return <NoContent context={context} {...config} />;
            default:
                throw new Error(`ContentFactory - Cannot find strategy '${strategy}'`);
        }
    }
}