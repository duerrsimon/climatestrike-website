import React from "react";
import {Menu, Row, Col} from "antd";
import {filter, includes, map, cloneDeep, remove, ceil} from "lodash";
import classNames from "classnames";

import {IContribution, IAppContext, II18nProps} from "../components/@def";
import {LanguageSelector} from "../components/Selector";
import {SocialLinks} from "../components/SocialLinks";
import {CsForm} from "../components/Form";
import {Link, withTranslation} from "../services/I18n";
import {IPage} from "../interfaces/model";


interface ICsAProps {
    page: IPage;
}
const CsA = withTranslation()<ICsAProps & II18nProps>(({page, t}) => {
    return <a className={page.id}>{t(page.id)}</a>;
});

const reducePages = (pages, location) => {
    const filtered = filter(pages, page => includes(page.linkLocations, location));
    const mapped = map(filtered, (page) => {
        return {
            id: page.id,
            slug: page.slug,
            menuItem: () => {return <CsA page={page} />;}
        };
    });
    return mapped;
};

const distributeToColumns = (items: any[], n: number) => {
    const toDistribute = cloneDeep(items);
    let i = n;
    const distributed = [];
    while (i > 0) {
        const nColumn = ceil(toDistribute.length / i);
        const columnItems = remove(toDistribute, (_item, index) => index < nColumn);
        distributed.push(columnItems);
        i--;
    }
    return distributed;
};

export class ContributionFactory {
    public create(strategy: string, context: IAppContext, config: any): IContribution {
        switch (strategy) {
            case "languages":
                return {
                    id: strategy, provider: () => <LanguageSelector items={config.languages} />
                };
            case "social":
                return {id: strategy, provider: () => <SocialLinks {...config} />};
            case "form":
                return {id: strategy, provider: () => <CsForm {...config} />};
            case "icon-menu-item":
                return {
                    id: strategy, provider: () => <Menu.Item key={strategy}>
                        <Link href={config.href}><a><img className="layout-home-icon" src={config.src} /></a></Link>
                    </Menu.Item>
                };
            case "menu-item":
                return {
                    id: strategy, provider: () => {
                        const pages = reducePages(context.pages, config.location);
                        return map(pages, (page, index) => {
                            return <Menu.Item key={"" + index} className={classNames({selected: page.id === context.activePage})}>
                                <Link href={"/" + page.slug}>{<div>{page.menuItem()}</div>}</Link>
                            </Menu.Item>;
                        });
                    }
                };
            case "link-item":
                return {
                    id: strategy, provider: () => {
                        const pages = reducePages(context.pages, config.location);
                        const nColumns = config.nColumns;
                        const columnItems = distributeToColumns(pages, nColumns);
                        return <Row type="flex" gutter={[32, 0]}>
                            {map(columnItems, (items, index) => {
                                return <Col key={"" + index} xs={24} lg={12}>
                                    {map(items, (page) => {
                                        return <Link key={page.id} href={page.slug}>{<div>{page.menuItem()}</div>}</Link>;
                                    })}
                                </Col>;
                            })}
                        </Row>;
                    }
                };
            default:
                throw new Error(`ContributionFactory - Cannot find strategy '${strategy}'`);
        }
    }
}