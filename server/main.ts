import next from "next";
import express from "express";
import bodyParser from "body-parser";
import nextI18NextMiddleware from "next-i18next/middleware";
import dotenv from "dotenv";

import NextI18NextInstance from "../services/I18n";
import {MailchimpService} from "./services/MailchimpService";


const port = parseInt(process.env.PORT || "3000", 10);
const dev = process.env.NODE_ENV !== "production";
const app = next({dev});
const handle = app.getRequestHandler();

if (dev) {
    dotenv.config();
}

(async () => {
    await app.prepare();
    const server = express();
    server.use(bodyParser.urlencoded({extended: true}));
    server.use(bodyParser.json());
    const $mc = new MailchimpService(
        process.env.MAILCHIMP_API_KEY,
        process.env.MAILCHIMP_LIST_ID,
        {email: "EMAIL", zip: "POSTLEITZA", language: "MMERGE5", firstname: "FNAME", lastname: "LNAME"}
    );
    server.post("/register-newsletter", (req, res) => {
        $mc.register(req.body)
            .then(() => {
                res.sendStatus(200);
            })
            .catch(() => {
                res.sendStatus(500);
            });
    });

    server.use(nextI18NextMiddleware(NextI18NextInstance));
    server.get("*", (req, res) => handle(req, res));
    await server.listen(port);

    console.log(`> Server listening at http://localhost:${port} as ${dev ? "development" : process.env.NODE_ENV}`);
})();