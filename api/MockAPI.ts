import {TObjectId} from "../interfaces/core";
import {IPageModelStrategy, IPage, IContentItem, ELinkLocation} from "../interfaces/model";
import {IHomeItemProps, IImgProps, IEnumerationItemProps, IHtmlProps, IIframeProps, ITileItemProps, ILinkSectionProps, IEventsProps} from "../components/@def";
import {FacebookAPI} from "./FacebookAPI";
import {sortBy} from "lodash";


export class MockAPI implements IPageModelStrategy {
    public async getPages() {
        return new Promise<IPage[]>(
            (resolve) => {
                resolve([
                    {id: "home", slug: "", linkLocations: [ELinkLocation.header], config: {rowModel: [1, 2, 2]}},
                    {id: "facts", slug: "facts", linkLocations: [ELinkLocation.header], config: {}},
                    {id: "movement", slug: "movement", linkLocations: [ELinkLocation.header], config: {}},
                    {id: "contact", slug: "contact", linkLocations: [ELinkLocation.footer], config: {}},
                    {id: "media", slug: "media", linkLocations: [ELinkLocation.footer], config: {}},
                    {id: "join", slug: "join", linkLocations: [ELinkLocation.header, ELinkLocation.footer], config: {}},
                    {id: "regio", slug: "regio", linkLocations: [ELinkLocation.footer], config: {}},
                    {id: "events", slug: "events", linkLocations: [ELinkLocation.header, ELinkLocation.footer], config: {}},
                    {id: "news", slug: "news", linkLocations: [ELinkLocation.header], config: {}},
                    {id: "donate", slug: "donate", linkLocations: [ELinkLocation.header, ELinkLocation.footer], config: {rowModel: [2]}},
                    {id: "about", slug: "about", linkLocations: [ELinkLocation.footnote], config: {}},
                    {id: "privacy", slug: "privacy", linkLocations: [ELinkLocation.footnote], config: {}}
                ]);
            });
    }

    public async getContent(pageId: TObjectId, languageId: TObjectId) {
        if (languageId === "de") {
            if (pageId == "/" || pageId === "home") {
                return new Promise<IContentItem[]>((resolve) => {
                    const nextStrike: IContentItem<IHomeItemProps> = {
                        id: "next-strike",
                        type: "home-item",
                        config: {
                            title: "Nächster Streik!",
                            subtitle: "Freitag, 29. November",
                            image: {
                                id: "next-strike-img",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                                position: "background"
                            },
                            link: {id: "next-strike-link", href: "/events", type: "more"}
                        }
                    };
                    const news: IContentItem<IHomeItemProps> = {
                        id: "news",
                        type: "home-item",
                        config: {
                            title: "News!",
                            description: "Wir sind vor allem junge Menschen, die sich um ihre eigene Zukunft, die ihrer Kinder und die des Planeten sorgen. Obwohl die wissenschaftlichen Fakten um den Treibhauseffekt und seine Folgen seit Jahrzehnten bekannt sind.",
                            image: {
                                id: "news-img",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                                position: "background",
                            },
                            link: {id: "news-link", href: "/news", type: "more"}
                        }
                    };
                    const why: IContentItem<IHomeItemProps> = {
                        id: "why",
                        type: "home-item",
                        config: {
                            title: "Warum wir streiken",
                            description: "Wir sind vor allem junge Menschen, die sich um ihre eigene Zukunft, die ihrer Kinder und die des Planeten sorgen. Obwohl die wissenschaftlichen Fakten um den Treibhauseffekt und seine Folgen seit Jahrzehnten bekannt sind.",
                            image: {
                                id: "why-img",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                                position: "background",
                            },
                            link: {id: "why-link", href: "/movement", type: "more"}
                        }
                    };
                    const facts: IContentItem<IHomeItemProps> = {
                        id: "facts",
                        type: "home-item",
                        config: {
                            title: "Facts",
                            description: "Wir sind vor allem junge Menschen, die sich um ihre eigene Zukunft, die ihrer Kinder und die des Planeten sorgen. Obwohl die wissenschaftlichen Fakten um den Treibhauseffekt und seine Folgen seit Jahrzehnten bekannt sind.",
                            image: {
                                id: "facts-img",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                                position: "background",
                            },
                            link: {id: "facts-link", href: "/movement", type: "more"}
                        }
                    };
                    const join: IContentItem<IHomeItemProps> = {
                        id: "join",
                        type: "home-item",
                        config: {
                            title: "Join",
                            description: "Wir sind vor allem junge Menschen, die sich um ihre eigene Zukunft, die ihrer Kinder und die des Planeten sorgen. Obwohl die wissenschaftlichen Fakten um den Treibhauseffekt und seine Folgen seit Jahrzehnten bekannt sind.",
                            image: {
                                id: "join-img",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                                position: "background",
                            },
                            link: {id: "join-link", href: "/join", type: "more"}
                        }
                    };
                    resolve([
                        nextStrike,
                        news,
                        why,
                        facts,
                        join,
                        nextStrike
                    ]);
                });
            }
            else if (pageId === "facts") {
                return new Promise<IContentItem[]>((resolve) => {
                    const nextStrike: IContentItem<IImgProps> = {
                        id: "fact-1",
                        type: "img",
                        config: {
                            image: {
                                id: "fact-1-img",
                                src: "/static/images/de_facts-1.svg",
                                color: "white",
                                position: "background"
                            },
                            link: {id: "fact-1-link", href: "/events", type: "more"}
                        }
                    };
                    resolve([
                        nextStrike
                    ]);
                });
            }
            else if (pageId === "join") {
                return new Promise<IContentItem[]>((resolve) => {
                    const thanks: IContentItem<ITileItemProps> = {
                        id: "thanks",
                        type: "tile-item",
                        config: {
                            title: "Danke",
                            subtitle: "DASS DU Dich für das Klima einsetzt!",
                            description: "Auch schon kleine Handlungen können Grosses bewirken!",
                            icon: {
                                id: "heart",
                                position: "left",
                                src: "/static/images/heart.svg"
                            },
                            image: {
                                id: "join-0-img",
                                position: "alternating",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                            }
                        }
                    };
                    const talk: IContentItem<IEnumerationItemProps> = {
                        id: "talk",
                        type: "enumeration-item",
                        config: {
                            title: "Rede mit Leuten in deinem Umfeld über die Klimakrise",
                            description: "Die Klimakrise geht jeden von uns etwas an. Umso mehr Leute darüber Bescheid wissen, umso grösser ist die Wahrscheinlichkeit dass wir es schaffen sie zu überwinden.",
                            image: {
                                id: "join-1-img",
                                position: "alternating",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                            },
                            link: {id: "join-1-link", href: "/facts", type: "facts"}
                        }
                    };
                    const join: IContentItem<IEnumerationItemProps> = {
                        id: "join",
                        type: "enumeration-item",
                        config: {
                            title: "Sei mit dabei",
                            description: "Komm an die Demos und Streiks! Alle sind willkommen, egal ob jung oder alt! Am besten nimmst du gleich deine ganze Familie und alle deine Freund*innen mit.",
                            image: {
                                id: "join-2-img",
                                position: "alternating",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                            },
                            link: {id: "join-2-link", href: "/events", type: "events"}
                        }
                    };
                    const network: IContentItem<IEnumerationItemProps> = {
                        id: "network",
                        type: "enumeration-item",
                        config: {
                            title: "Vernetze dich mit anderen Menschen aus deiner Region",
                            description: "Trete einem lokalen Klimastreik-Chat bei. Erfahre immer alles aus erster Hand und vernetze dich mit Menschen in deiner Nähe. Das Klima braucht dich!",
                            image: {
                                id: "join-3-img",
                                position: "alternating",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                            },
                            link: {id: "join-3-link", href: "/chat", type: "join chat"}
                        }
                    };
                    const organize: IContentItem<IEnumerationItemProps> = {
                        id: "organize",
                        type: "enumeration-item",
                        config: {
                            title: "Hilf bei der Organisation",
                            description: "Um uns bei der Organisation zu unterstützen, trittst du am besten deiner Regionalgruppe bei. Dort wirst du weitere Informationen erhalten, wie du mithelfen kannst",
                            image: {
                                id: "join-4-img",
                                position: "alternating",
                                src: "/static/images/49151584486_c18a65b564_c.jpg",
                            },
                            link: {id: "join-4-link", href: "/chat", type: "join chat"}
                        }
                    };
                    const wiki: IContentItem<ILinkSectionProps> = {
                        id: "wiki",
                        type: "link-section",
                        config: {
                            title: "wiki",
                            description: "HIER FINDEST DU ALLE INFOS ÜBER DIE STRUKTUR UNSERER BEWEGUNG",
                            link: {id: "join-5-link", href: "https://de.climatestrike.ch", type: "Wiki"}
                        }
                    };
                    const download: IContentItem<ILinkSectionProps> = {
                        id: "download",
                        type: "link-section",
                        config: {
                            title: "download",
                            description: "Flyer, Logos & co.",
                            link: {id: "join-5-link", href: "/downloads", type: "Downloads"}
                        }
                    };
                    resolve([
                        thanks,
                        talk,
                        join,
                        network,
                        organize,
                        wiki,
                        download
                    ]);
                });
            }
            else if (pageId === "donate") {
                return new Promise<IContentItem[]>((resolve) => {
                    const description: IContentItem<IHtmlProps> = {
                        id: "donation-message",
                        type: "html",
                        config: {
                            html: "<p>Im Moment sind wir auf Spenden angewiesen, um kleinere Dinge, wie beispielsweise Flyer, Plakate oder diese Website zu finanzieren.Viele von uns sind Schüler*innen oder Student*innen, deshalb ist unser finanzieller Spielraum beschränkt. Wir freuen uns über jede Spende!</p> <p>IBAN: CH28 0839 0036 0277 1000 2 <br> BIC: ABSOCH22XXX</p> <p>Climatestrike Switzerland<br>Hinterdorfstrasse 12<br>8635 Dürnten</p>"
                        }
                    };
                    const item: IContentItem<IIframeProps> = {
                        id: "donorbox",
                        type: "iframe",
                        config: {
                            src: "https://donorbox.org/embed/climatestrike-ch"
                        }
                    };
                    resolve([
                        description,
                        item
                    ]);
                });
            }
            else if (pageId === "events") {
                const fbApi = new FacebookAPI();
                const upcoming = await fbApi.getUpcomingEvents();
                const past = await fbApi.getPastEvents();
                return new Promise<IContentItem[]>((resolve) => {
                    const events: IContentItem<IEventsProps> = {
                        id: "event-1",
                        type: "events",
                        config: {
                            items: sortBy(past.concat(upcoming), event => event.startTime)
                        }
                    };
                    resolve([events]);
                });
            }
            return new Promise<IContentItem<IImgProps>[]>((resolve) => {
                resolve([{
                    id: "no-content-available",
                    type: "no-content-available",
                    config: {
                        image: {
                            id: "no-content-img",
                            position: "left",
                            caption: "Photo by Vlad Tchompalov on Unsplash",
                            src: "/static/images/vlad-tchompalov-dQkXoqQLn40-unsplash.jpg"
                        },
                        link: {
                            id: "home",
                            href: ""
                        }
                    }
                }]);
            });
        }
    }
}