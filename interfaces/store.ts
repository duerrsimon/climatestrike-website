import {TObjectId} from "./core";
import {IServices} from "./services";

export interface IStoreState {
    $services: IServices;
    language: TObjectId;
}

export enum EActionType {
    set = "set",
    setLanguage = "setLanguage"
}

export interface IStoreAction<T = any> {
    type: EActionType;
    payload: T
}

/**
 * @param {boolean} isServer indicates whether it is a server side or client side
 * @param {Request} req NodeJS Request object (not set when client applies initialState from server)
 * @param {Request} res NodeJS Request object (not set when client applies initialState from server)
 * @param {boolean} debug User-defined debug mode param
 * @param {string} storeKey This key will be used to preserve store in global namespace for safe HMR
 */
export interface IMakeStoreOptions {
    isServer: boolean;
    req: Request;
    res: Response;
    debug: boolean;
    storeKey: string;
}