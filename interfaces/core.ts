export interface IDictionary<T> {
    [index: string]: T;
}

export type TObjectId = string;

export interface INamedObject {
    id: TObjectId;
    name: string;
}

export interface IUrlObject {
    id: TObjectId;
    url: string;
}

export interface IFormField {
    key: string;
    type: string;
    typeRule: string;
    pattern?: string;
    placeholder?: string;
    icon?: string;
    required: boolean;
    hideBeforeFocus: boolean;
}