import {TObjectId, IDictionary} from "./core";

export interface IStrategy<T = any> {
    strategy: string;
    config: T;
}

export interface ITranslationDataSource {
    // TODO
}

export interface IDataSourcConfiguration {
    content: IStrategy[];
    translations: ITranslationDataSource;
}

export interface IMenuConfig {
    contributions: IStrategy[];
}

export interface ISubMenuConfig {
    contributions: IStrategy[];
}

export interface IFooterConfig {
    contributions: IStrategy[];
}

export interface ILanguageConfig {
    languages: TObjectId[];
    default: TObjectId;
    paths?: IDictionary<string>;
}

export interface IColorConfig {
    default: string[];
}

export interface IConfiguration {
    dataSources: IDataSourcConfiguration;
    menu: IMenuConfig;
    submenu: ISubMenuConfig;
    footer: IFooterConfig;
    languages: ILanguageConfig;
    colors: IColorConfig;
}