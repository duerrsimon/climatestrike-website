import {TObjectId} from "./core";


export interface IContentItem<T = any> {
    id: TObjectId;
    type: string;
    config: T;
}

export enum ELinkLocation {
    header = "header",
    footer = "footer",
    footnote = "footnote"
}

export interface IPageConfig {
    rowModel?: number[];
}

export interface IPage {
    id: TObjectId;
    slug: string;
    linkLocations: ELinkLocation[]; // e.g. ["menu", "footer"]
    config: IPageConfig;
}

export interface IPageModelStrategy {
    getPages(): Promise<IPage[]>;
    getContent(pageid: TObjectId, language: TObjectId): Promise<IContentItem[]>;
}

export interface IPageModel extends IPageModelStrategy {
    setStrategy(strategy: IPageModelStrategy): void;
}