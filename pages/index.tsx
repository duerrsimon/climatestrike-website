import {NextPage} from "next";

import {IPageProps} from "../components/@def";
import PageWrapper from "../components/PageWrapper";
import {AbstractPage} from "../components/AbstractPage";


const IndexPage: NextPage<IPageProps> = ({items, pages, activePage}) => {
    return AbstractPage({
        items,
        pages,
        pageId: activePage
    });
};

export default PageWrapper(IndexPage);
