import React from "react";
import classNames from "classnames";
import {Row, Col} from "antd";

import {ITileItemProps, IPageContextProps} from "./@def";
import {renderImage, renderDescription, renderTitle, renderSubtitle} from "./BaseComponents";


export const TileItem: React.FC<ITileItemProps & IPageContextProps> = (props) => {
    const {context, title, subtitle, description, icon, image} = props;
    const className = classNames({[context.itemId]: true, "tile-item": true});
    return <Row className={className} type="flex" gutter={[24, 24]} >
        <Col className="tile-container" xs={24} sm={16}>
            <Row type="flex" gutter={[24, 24]}>
                <Col className="tile-icon" xs={24} sm={8}>{renderImage(icon)}</Col>
                <Col className="tile-headers" xs={24} sm={16}>
                    {[renderTitle(title), renderSubtitle(subtitle)]}
                </Col>
            </Row>
            <Row className="tile-description">
                {renderDescription(description)}
            </Row>
        </Col>
        <Col className="tile-image" xs={0} sm={8}>{renderImage(image)}</Col>
    </Row>;
};