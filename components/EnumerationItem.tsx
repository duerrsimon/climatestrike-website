import React from "react";
import classNames from "classnames";
import {map, filter, findIndex} from "lodash";
import {Row, Col} from "antd";

import {TObjectId} from "../interfaces/core";
import {IContentItem} from "../interfaces/model";
import {IEnumerationItemProps, IPageContextProps} from "./@def";
import {renderDescription, renderLink, renderImage, renderEnumeration} from "./BaseComponents";


const getEnumeration = (itemId: TObjectId, items: IContentItem[]) => {
    const enumItems = filter(items, item => item.type == "enumeration-item");
    const itemIndex = findIndex(enumItems, item => item.id == itemId);
    return itemIndex + 1;
};

const EnumerationContent = ({n, title, description, link}) => {
    const renderers = [];
    renderers.push(() => <span>
        {renderEnumeration(n)}
        <span className="enumeration-title">{title}</span>
    </span>);
    if (description) renderers.push(() => renderDescription(description));
    if (link) renderers.push(() => renderLink(link));
    return <div className="enumeration-content">
        {map(renderers, renderer => renderer())}
    </div>;
};

export const EnumerationItem: React.FC<IEnumerationItemProps & IPageContextProps> = (props) => {
    const {context, title, description, image, link} = props;
    const className = classNames({[context.itemId]: true, "enumeration-item": true});
    const renderers = [() => <Col xs={24} sm={8}>{renderImage(image)}</Col>];
    const enumeration = getEnumeration(context.itemId, context.items);
    const renderContent = () => <Col xs={24} sm={16}><EnumerationContent n={enumeration} title={title} description={description} link={link} /></Col>;
    if (enumeration % 2 === 0 || process.browser && window.screen.width < 576) {
        renderers.unshift(renderContent);
    }
    else {
        renderers.push(renderContent);
    }
    return <Row className={className} type="flex" gutter={[24, 24]}>
        {map(renderers, renderer => renderer())}
    </Row>;
};