import React from "react";
import {map, forEach, cloneDeep, merge, find} from "lodash";
import {Row, Col} from "antd";
import classNames from "classnames";

import {IPageContext, IAbstractPageProps} from "./@def";
import {IContentItem} from "../interfaces/model";
import {ContentFactory} from "../factories/ContentFactory";
import {ColorService} from "../services/ColorService";


export const getRowModel = (items: IContentItem[], config: number[]) => {
    const rowModel: IContentItem[][] = [];
    let currentRow = [];
    let configIndex = 0;
    forEach(items, (item, index) => {
        currentRow.push(item);
        if (currentRow.length == config[configIndex]) {
            rowModel.push(cloneDeep(currentRow));
            currentRow = [];
            configIndex = (configIndex + 1) % config.length;
        }
        if (index === items.length - 1 && currentRow.length !== 0) {
            rowModel.push(currentRow);
        }
    });
    return rowModel;
};

export const AbstractPage: React.FC<IAbstractPageProps> = (props) => {
    const factory = new ContentFactory();
    const $color = new ColorService();
    const {pageId, items, pages} = props;
    const page = find(pages, (_page) => _page.id == pageId);
    const context: IPageContext = {itemId: null, pageId, items: items, rowItems: []};
    const rowModel = getRowModel(items, page.config.rowModel ? page.config.rowModel : [1]);
    return <div key={pageId} className={classNames({page: true, [pageId]: true})}>
        {map(rowModel, (row, index) => <Row key={index} gutter={[24, 24]} type="flex">
            {map(row, item => <Col key={"" + index + "-" + item.id} xs={24} lg={24 / row.length}>
                {factory.create(
                    item.type,
                    merge({image: {color: $color.getNextDefault()}}, item.config),
                    merge({}, context, {rowItems: row, itemId: item.id})
                )}
            </Col>)}
        </Row>)}
    </div>;
};