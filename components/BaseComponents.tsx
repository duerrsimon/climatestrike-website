import React from "react";

import {IButtonProps, IImageConfig} from "./@def";
import Button from "./Button";


export const renderTitle = (text: string) => <h1 key="title"><span>{text}</span></h1>;
export const renderSubtitle = (text: string) => <h2 key="subtitle"><span>{text}</span></h2>;
export const renderDescription = (text: string) => <p key="description">{text}</p>;
export const renderLink = (link: IButtonProps) => <Button key="button" {...link} />;
export const renderEnumeration = (n: number) => <span className="enumeration">{n + "."}</span>;
export const renderImage = (image: IImageConfig) => <div className="image" style={{backgroundImage: `url('${image.src}')`}} />;