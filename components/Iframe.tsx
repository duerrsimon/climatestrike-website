import React from "react";
import classNames from "classnames";

import {IIframeProps, IPageContextProps} from "./@def";


export const Iframe: React.FC<IIframeProps & IPageContextProps> = ({context, src}) => {
    const className = classNames({[context.itemId]: true, iframe: true});
    return <div className={className}>
        <iframe src={src} />
    </div>;
};