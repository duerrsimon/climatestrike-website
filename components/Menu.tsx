import * as React from "react";
import {map} from "lodash";
import {Menu, Icon} from "antd";

import {IMenuProps} from "./@def";


export const DesktopMenu: React.FC<IMenuProps> = ({contributions}) => {
    return <Menu
        className="desktop-menu"
        mode="horizontal"
        overflowedIndicator={<Icon type="menu" />}
    >
        {map(contributions, (contribution) => {
            return contribution.provider();
        })}
    </Menu>;
};