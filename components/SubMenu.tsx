import * as React from "react";
import {map, isFunction} from "lodash";
import classNames from "classnames";

import {ISubMenuProps} from "./@def";


export const SubMenu: React.FC<ISubMenuProps> = ({contributions}) => {
    return <div className="sub-menu">
        {map(contributions, (contribution) => {
            const className = classNames({"contribution-container": true, [contribution.id]: true});
            return <div key={contribution.id} className={className}>
                {isFunction(contribution.provider) ? contribution.provider() : null}
            </div>;
        })}
    </div>;
};