import * as React from "react";
import {map} from "lodash";
import {Icon} from "antd";

import {IUrlObject} from "../interfaces/core";


interface ISocialLinksProps {
    social: IUrlObject[];
}

export const SocialLinks: React.FC<ISocialLinksProps> = ({social}) => {
    return <div className="social-links">
        {map(social, (socialItem) => <a key={socialItem.id} href={socialItem.url} target="_blank" rel="noopener noreferrer">
            <Icon className="social-link-icon" type={socialItem.id} />
        </a>)}
    </div>;
};

