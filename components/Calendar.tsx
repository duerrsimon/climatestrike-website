import React from "react";
import classNames from "classnames";
import {Calendar as AntCalendar} from "antd";
import moment from "moment";

import {IEventsProps, IPageContextProps, II18nProps} from "./@def";
import {withTranslation} from "../services/I18n";


const _Calendar: React.FC<IEventsProps & IPageContextProps & II18nProps> = (props) => {
    const language = props.i18n.language;
    const {context} = props;
    moment.locale("ch-" + language, {
        week: {dow: 1, doy: 4}
    });
    const className = classNames({[context.itemId]: true});
    return <AntCalendar className={className} />;
};

export const Calendar = withTranslation()(_Calendar);