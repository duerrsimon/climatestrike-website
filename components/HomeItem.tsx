import * as React from "react";
import classNames from "classnames";
import {map} from "lodash";

import {IHomeItemProps, IPageContextProps} from "./@def";
import {renderTitle, renderSubtitle, renderDescription, renderLink} from "./BaseComponents";


// TODO: Better naming
export const HomeItem: React.FunctionComponent<IHomeItemProps & IPageContextProps> = (props) => {
    const {title, subtitle, description, image, link} = props;
    const className = classNames({"home-item": true}, ["row-of-" + props.context.rowItems.length, props.context.itemId]);
    const color = image ? image.color : null;
    const style = {
        backgroundImage: image
            && `linear-gradient(${color}, ${color}), url('${image.src}')`
    };
    const renderers = [];
    if (title) renderers.push(() => renderTitle(title));
    if (subtitle) renderers.push(() => renderSubtitle(subtitle));
    if (description) renderers.push(() => renderDescription(description));
    if (link) renderers.push(() => renderLink(link));

    return <div
        className={className}
        style={style} >
        {map(renderers, renderer => renderer())}
    </div >;
};

