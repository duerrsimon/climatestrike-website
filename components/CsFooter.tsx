import * as React from "react";
import {Row, Col} from "antd";
import {map} from "lodash";

import {IFooterProps} from "./@def";

export const CsFooter: React.FC<IFooterProps> = ({contributions}) => {
    return <Row className="cs-footer" type="flex" justify="space-between" gutter={[24, 24]}>
        {map(contributions, contribution => <Col key={contribution.id} className={contribution.id}>{contribution.provider()}</Col>)}
    </Row>;
};