import * as React from "react";
import {map, filter, some} from "lodash";
import {Form, Icon, Input, Button, message} from "antd";
import {FormComponentProps} from "antd/lib/form";

import {IFormField} from "../interfaces/core";
import {withTranslation} from "../services/I18n";
import {II18nProps} from "./@def";
import {HttpService} from "../services/HttpService";


interface IFormProps {
    submitText: string;
    submitUrl: string;
    fields: IFormField[];
}

class CustomForm extends React.Component<IFormProps & FormComponentProps & II18nProps> {
    private $http = HttpService.instance();

    public componentDidMount() {
        // To disable submit button at the beginning.
        this.props.form.validateFields();
    }

    public handleSubmit = e => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                const submitValues = {...values, language: this.props.i18n.language};
                this.$http.postd(this.props.submitUrl, submitValues)
                    .then(() => {
                        message.success(this.props.t("Sucessfully registered for newsletter"));

                        this.props.form.resetFields();
                    })
                    .catch((err) => {
                        message.error(this.props.t("Error registering for newsletter. Please try again later"));
                        console.error("Error submitting form: " + err);
                    });
            }
            else {
                console.error("Error in form submission: " + err);
            }
        });
    };

    public render() {
        const {getFieldDecorator, getFieldError, getFieldValue, isFieldsTouched} = this.props.form;
        const {t, fields, submitText} = this.props;
        const visibleFields = filter(fields, field => !field.hideBeforeFocus);
        const visibleFieldKeys = map(visibleFields, field => field.key);
        const isFocused = isFieldsTouched(visibleFieldKeys);
        const formFields = isFocused ? fields : visibleFields;
        const errors = [];
        const layout = "vertical";

        // Only show error after a field is touched.
        return <Form layout={layout} onSubmit={this.handleSubmit} >
            {map(formFields, (field) => {
                const error = isFocused && (getFieldError(field.key) || (field.required && !getFieldValue(field.key) && t("This field is required!")));
                errors.push(error);
                return <Form.Item key={field.key} validateStatus={error ? "error" : ""} help={error || ""} >
                    {getFieldDecorator(field.key, {rules: [{type: field.pattern ? null : field.typeRule, message: t("Please enter a valid " + field.key)}, {required: field.required, message: t("This field is required!")}]})(
                        <Input
                            type={field.type}
                            prefix={<Icon type={field.icon} style={{color: "rgba(0,0,0,.25)"}} />}
                            placeholder={t(field.placeholder)}
                        />,
                    )}
                </Form.Item>;
            })}
            <Form.Item key="submit">
                <Button type="primary" htmlType="submit" disabled={!isFocused || some(errors)}>
                    {t(submitText)}
                </Button>
            </Form.Item>
        </Form >;
    }
}

export const CsForm = Form.create({name: "customform"})(withTranslation()<IFormProps & II18nProps>(CustomForm));