import React from "react";
import classNames from "classnames";
import {map} from "lodash";

import {ILinkSectionProps, IPageContextProps} from "./@def";
import {renderTitle, renderDescription, renderLink} from "./BaseComponents";


export const LinkSection: React.FC<ILinkSectionProps & IPageContextProps> = (props) => {
    const {context, title, description, link} = props;
    const className = classNames({[context.itemId]: true, "link-section": true});
    const renderers = [
        () => renderTitle(title),
        () => renderDescription(description),
        () => renderLink(link)
    ];
    return <div className={className} >
        {map(renderers, renderer => renderer())}
    </div>;
};