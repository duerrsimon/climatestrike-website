import React from "react";
import classNames from "classnames";

import {IHtmlProps, IPageContextProps} from "./@def";


export const HTML: React.FC<IHtmlProps & IPageContextProps> = ({context, html}) => {
    const className = classNames({[context.itemId]: true, html: true});
    return <div className={className} dangerouslySetInnerHTML={{__html: html}} />;
};