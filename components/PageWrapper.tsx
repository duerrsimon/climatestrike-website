import * as React from "react";
import {NextPage, NextPageContext} from "next";

import Layout from "./CsLayout";
import {config} from "../config/config";
import {PageModel, MockModelStrategy} from "../model/PageModel";
import {MockAPI} from "../api/MockAPI";
import {IPageWrapperProps} from "./@def";
import NextI18NextInstance from "../services/I18n";
// import {get, find} from "lodash";
// import {isString} from "util";


const PageWrapper = (Page: NextPage<any>) => {
    return class Pg extends React.Component<IPageWrapperProps> {
        static async getInitialProps(args: NextPageContext) {
            const currentLanguage = args.req ? (args.req as any).language : NextI18NextInstance.i18n.language;
            const pageModel = new PageModel();
            // FIXME: Use new API once implemented
            const mockApi = new MockAPI();
            pageModel.setStrategy(new MockModelStrategy(mockApi));
            // TODO: Find proper solution
            const pages = await pageModel.getPages();

            let pageId = args.pathname.replace("/", "");
            if (args.pathname === "/") {
                pageId = "home";
            }
            // Super hacky workaround for for dynamic catch-all route.
            // See https://github.com/zeit/next.js/issues/9081 for more info / check if resolved
            // The page flickers and the request returns a 404. Some more investigation is needed.
            // For it to work rename __error.tsx to _error.tsx
            // const route = get(args.req, "params.0");
            // else if (route && isString(route)) {
            //     const slug = route.replace("/", "");
            //     if (pageId === "_error" && find(pages, (page) => page.slug === slug)) {
            //         console.log("Redirecting from _error to " + slug);
            //         pageId = slug;
            //     }
            // }
            const content = await pageModel.getContent(pageId, currentLanguage);
            const wrapperProps: IPageWrapperProps = {
                title: pageId,
                activePage: pageId,
                pages: pages,
                menu: config.menu,
                submenu: config.submenu,
                footer: config.footer,
                items: content
            };
            return {
                ...wrapperProps,
                ...(Page.getInitialProps ? await Page.getInitialProps(args) : null)
            };
        }

        public render() {
            return (
                <Layout {...this.props}>
                    <Page
                        {...this.props}
                    />
                </Layout>
            );
        }
    };
};

export default PageWrapper;
