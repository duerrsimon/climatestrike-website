import {TObjectId} from "../interfaces/core";
import {IPage, IContentItem} from "../interfaces/model";
import {IMenuConfig, ISubMenuConfig, IFooterConfig} from "../interfaces/config";
import {WithTranslation} from "next-i18next";


export interface INavPage {
    id: TObjectId;
    route: string;
}

export interface INavigationModel {
    pages: INavPage[];
}

export interface IMenuPage {
    id: TObjectId;
    slug: string;
    menuItem: React.FC<{}>;
}

export interface IMenuProps {
    activePage: TObjectId;
    contributions: IContribution[];
}

export interface IAppContext {
    activePage: TObjectId;
    pages: IPage[];
}

export interface IContribution {
    id: TObjectId;
    provider: () => React.ReactNode;
}

export interface ISubMenuProps {
    contributions: IContribution[]
}

export interface IFooterProps {
    contributions: IContribution[]
}

export interface IAbstractPageProps {
    pageId: TObjectId;
    pages: IPage[];
    items: IContentItem[];
}

export interface IPageProps {
    activePage: TObjectId;
    pages: IPage[];
    items: IContentItem[];
}

export interface IPageWrapperProps extends IPageProps {
    title?: string;
    pages: IPage[];
    menu: IMenuConfig;
    submenu: ISubMenuConfig;
    footer: IFooterConfig;
    children?: React.ReactNode;
}

export interface IPageContext {
    itemId: TObjectId;
    pageId: TObjectId;
    items: IContentItem[];
    rowItems: IContentItem[];
}

export interface IPageContextProps {
    context: IPageContext;
}

export interface II18nProps extends WithTranslation {
}

export interface IButtonProps {
    type?: string;
    text?: string;
    href?: string;
    onClick?: () => void;
}

export interface IImageConfig {
    id: TObjectId;
    src: string;
    position: "background" | "left" | "right" | "alternating"; // Not yet supported
    color?: string;
    caption?: string;
}

export interface ILinkConfig {
    id: TObjectId;
    href: string;
    type?: string;
    text?: string;
}

export interface IHomeItemProps {
    title?: string;
    subtitle?: string;
    description?: string;
    image?: IImageConfig;
    link?: ILinkConfig;
}

export interface IImgProps {
    image: IImageConfig;
    link?: ILinkConfig;
}

export interface IHtmlProps {
    html: string;
}

export interface IIframeProps {
    src: string;
}

export interface ITileItemProps {
    title: string;
    subtitle: string;
    description: string;
    icon: IImageConfig;
    image: IImageConfig;
}

export interface IEnumerationItemProps {
    title: string;
    description?: string;
    image?: IImageConfig;
    link?: ILinkConfig;
}

export interface ILinkSectionProps {
    title: string;
    description: string;
    link: ILinkConfig;
}

export interface IEvent {
    description: string;
    location: string;
    lat: string;
    lon: string;
}

export interface IEventItem {
    id: string;
    title: string;
    startTime: string;
    endTime: string;
    description: string;
    events: IEvent[];
}

export interface IEventsProps {
    items: IEventItem[];
}
