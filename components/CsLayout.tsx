import * as React from "react";
import Head from "next/head";
import {map} from "lodash";
import {Layout} from "antd";

import "../styles/styles.less";
import {DesktopMenu} from "./Menu";
import {SubMenu} from "./SubMenu";
import {CsFooter} from "./CsFooter";
import {ContributionFactory} from "../factories/ContributionFactory";
import {IPageWrapperProps, II18nProps} from "./@def";
import {withTranslation} from "../services/I18n";

const {Header, Content, Footer} = Layout;


const CsLayout: React.FC<IPageWrapperProps & II18nProps> = (props) => {
    const {title = "Climatestrike", activePage, pages, menu, submenu, footer, children} = props;

    const appContext = {activePage, pages};

    const contributionFactory = new ContributionFactory();
    const menuContributions = map(menu.contributions, (contribution) => contributionFactory.create(contribution.strategy, appContext, contribution.config));
    const submenuContributions = map(submenu.contributions, (contribution) => contributionFactory.create(contribution.strategy, appContext, contribution.config));
    const footerContributions = map(footer.contributions, (contribution) => contributionFactory.create(contribution.strategy, appContext, contribution.config));

    return (
        <div className="root">
            <Head>
                <title>{props.t(title)}</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <link rel="shortcut icon" href="/static/images/favicon.ico" />
            </Head>
            <Layout className="layout">
                <Header className="navigation-header">
                    <DesktopMenu activePage={activePage} contributions={menuContributions} />
                    {/* FIXME: Add mobile menu for < 768? px */}
                </Header>
                {/* FIXME: Add submenu to mobile header as well */}
                {submenu.contributions.length ? <Header className="subnavigation-header">
                    <SubMenu contributions={submenuContributions}></SubMenu>
                </Header> : null}
                <Content className="content">{children}</Content>
                <Footer className="footer">
                    <CsFooter contributions={footerContributions} />
                </Footer>
            </Layout>
        </div>
    );
};

export default withTranslation()<IPageWrapperProps & II18nProps>(CsLayout);
