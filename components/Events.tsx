import React, {useState} from "react";
import classNames from "classnames";
import {Input, Row} from "antd";
import {first, map} from "lodash";

import {IEventsProps, IPageContextProps, II18nProps} from "./@def";
import {Selector} from "./Selector";
import {withTranslation} from "../services/I18n";
import {ContentFactory} from "../factories/ContentFactory";


const _Events: React.FC<IEventsProps & IPageContextProps & II18nProps> = (props) => {
    const views = ["timeline", "calendar"];
    const [view, setView] = useState(first(views));
    const [zipFilter, setZipFilter] = useState(null);
    const typeFilters = ["all", "demo", "meetups", "festivals", "discussions", "info"];
    const [typeFilter, setTypeFilter] = useState(first(typeFilters));

    if (zipFilter) console.log(zipFilter);

    const {context, t} = props;
    const className = classNames({[context.itemId]: true});
    const contentFactory = new ContentFactory();

    return <div className={className}>
        <Row key="events-selectors" className="events-selectors" type="flex">
            <Selector
                items={map(views, (id) => ({id, name: t(id)}))}
                selected={[view]}
                onClick={(id) => setView(id)}
                className="events-view-selector"
            />
            <Input className="zip-input" type="number" pattern={"[0-9]{4}"} min="1000" max="9999" placeholder={t("ZIP")} allowClear={true} onChange={(e) => {
                let value = e.target.value;
                setZipFilter(value);
            }} />
            <Selector
                items={map(typeFilters, (id) => ({id, name: t(id)}))}
                selected={[typeFilter]}
                onClick={(id) => setTypeFilter(id)}
                className="events-type-selector"
            />
        </Row>
        <Row key="event-content">
            {contentFactory.create(view, props, context)}
        </Row>
    </div>;
};

export const Events = withTranslation()(_Events);