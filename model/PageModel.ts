import * as _ from "lodash";

import {TObjectId} from "../interfaces/core";
import {IWordPressAPI} from "../interfaces/api";
import {IPageModel, IPageModelStrategy, IPage, IContentItem} from "../interfaces/model";


export class PageModel implements IPageModel {
    private strategy: IPageModelStrategy;

    constructor(strategy?: IPageModelStrategy) {
        if (strategy) this.strategy = strategy;
    }

    public setStrategy(strategy: IPageModelStrategy) {
        this.strategy = strategy;
    }

    public getPages() {
        if (this.strategy) {
            return this.strategy.getPages();
        }
        throw new Error("PageModel - Missing strategy");
    }

    public getContent(pageId: TObjectId, language: TObjectId) {
        if (this.strategy) {
            return this.strategy.getContent(pageId, language);
        }
        throw new Error("PageModel - Missing strategy");
    }
}

export class MockModelStrategy implements IPageModelStrategy {
    constructor(private delegate: IPageModelStrategy) {}

    public async getPages() {
        return this.delegate.getPages();
    }

    public async getContent(pageId: TObjectId, language: TObjectId) {
        return this.delegate.getContent(pageId, language);
    }
}

export class WordPressPageModelStrategy implements IPageModelStrategy {
    constructor(private api: IWordPressAPI) {}

    public async getPages(): Promise<IPage[]> {
        try {
            const pages = await this.api.getPages();
            const sorted = _.sortBy(pages, (page) => page.menu_order);
            return _.map(sorted, (page) => ({id: "" + page.id, slug: page.slug, linkLocations: [], config: {}}));
        }
        catch (err) {
            throw new Error("WordPressPageModelStrategy - Error retrieving pages: " + err);
        }
    }

    public async getContent(pageId: TObjectId): Promise<IContentItem[]> {
        try {
            const categories = await this.api.getCategories();
            const pageCategory = _.find(categories, (category) => _.toLower(category.name) === _.toLower(pageId));
            if (pageCategory) {
                const posts = await this.api.getPosts(pageCategory.id);
                return _.map(posts, (post => ({id: "" + post.id, type: "post", config: post.content.rendered})));
            }
            return [];
        }
        catch (err) {
            throw new Error("WordPressPageModelStrategy - Error retrieving pages: " + err);
        }
    }
}