# General information
This project contains the code for the new climatestrike.ch website, implementing the design proposed [here](https://www.figma.com/proto/gS764DdLo3IkDJgHfni6fd/Climatestrike-Website-new-Prototype?node-id=260%3A0&viewport=299%2C362%2C0.019355816766619682&scaling=scale-down).

# Getting started
```
git clone https://gitlab.com/omg_me/climatestrike-website.git
cd climatestrike-website
npm i
```

For development, use:
```
npm run dev
```

For a production install, use
```
npm run build
# Temporary solution to keep the process running in the background
NODE_ENV=production nohup npm run dev > /dev/null &
```

# Contributions
See [CONTRIBUTING](CONTRIBUTING.md).

# License
See [LICENSE](LICENSE.md).