import {IServices} from "../interfaces/services";
import {ColorService} from "./ColorService";
import {HttpService} from "./HttpService";

export class Services implements IServices {
    private static _instance: IServices;

    public static instance(): IServices {
        if (!Services._instance) {
            Services._instance = new Services();
        }
        return Services._instance;
    }

    public $color = ColorService.instance();
    public $http = HttpService.instance();
}
