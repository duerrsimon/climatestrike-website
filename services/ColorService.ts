import {config} from "../config/config";

import {IColorService} from "../interfaces/services";

export class ColorService implements IColorService {
    private static _instance: IColorService;
    private defaultColors = config.colors.default;
    private currentDefaultIndex = 0;

    public static instance(): IColorService {
        if (!ColorService._instance) {
            ColorService._instance = new ColorService();
        }
        return ColorService._instance;
    }

    public getDefault(index: number) {
        return this.defaultColors[index % this.defaultColors.length];
    }

    public getNextDefault() {
        this.currentDefaultIndex = (this.currentDefaultIndex + 1) % this.defaultColors.length;
        return this.defaultColors[this.currentDefaultIndex];
    }

    public getDefaultPalette() {
        return this.defaultColors;
    }
}